print("Welcome to text adventure." )

answer = input("Would you like instructions?, 'Yes' or 'No'")
if answer == 'Yes':
    print("Ok, hold onto your horses")
elif answer == 'No':
    print("Too bad, you are getting instructions")
else:
    print("game over")
    quit()

if answer == 'Yes' or answer == 'No':
    print("Your mission is to locate the pot of gold at the end of the rainbow.")

option1 = input('You are at the fork in the road. Where would you like to go? Go to the road on the "left" or the road on the "right".').lower()

if option1 == "left":
    option2 = input('You have arrived at the local airport. There is a cave at the end of the rainbow. There is a car rental agency centrally located in the airport. Would you like to "wait" for a rental car. Would you like to "walk" during the adventure.').lower()
    if option2 == "wait":
        option3 = input('You made it to the end of the rainbow. There is a cave with 3 access points. One black, one orange, and one purple. Which of these do you choose?').lower()
        if option3 == "black":
            print("It's blazing hot. Sorry")
        elif option3 == "orange":
            print("You found the pot of gold at the end of the rainbow!!!")
        elif option3 == "purple":
            print("You get chased away by grizzly bear. Sorry, Adventure over")
        else:
            print("You went through an access point that is not there. Sorry")
    else:
        print("You sprained your ankle during the walk. Sorry. Adventure over")
else:
    print("You ran into an inevitable roadblock. Sorry. Adventure over.")



