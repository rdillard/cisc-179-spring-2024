
intro = "Welcome to text adventure.\n"
start = "Would you like instructions?, '[y]es' or '[n]o'\n"
resp = "yes\n"
str1 = "Ok, hold onto your horses\n"
str2 = "Too bad, you are getting instructions\n"
str3 = "game over\n"
str4 = "Your mission is to locate the pot of gold at the end of the rainbow.\n"
direction = 'You are at the fork in the road. Where would you like to go? Go to the road on the "[l]eft" or the road on the "[r]ight".\n'
resp3 = "left\n"
transport = 'You have arrived at the local airport. There is a cave at the end of the rainbow. There is a car rental agency centrally located in the airport. Would you like to "[p]atiently wait" for a rental car. Would you like to "[w]alk" during the adventure.\n'
resp4 = "patiently wait\n"
decisions = "You made it to the end of the rainbow. There is a cave with 3 access points. One [b]lack, one [o]range, and one [p]urple. Which of these do you choose?\n"
resp5 = "black\n"
str5 = "It's blazing hot. Sorry\n"
resp6 = "orange\n"
str6 = "You found the pot of gold at the end of the rainbow!!!\n"
resp7 = "purple\n"
str7 = "You get chased away by grizzly bear. Sorry, Adventure over.\n"
str8 = "You went through an access point that is not there. Sorry\n"
almost_there = 'You are near the end of your adventure. Would you like to "[c]ontinue" for more gold or go "[h]ome" with what you have?\n'
resp8 = "home"
str9 = "Great, you get to go home with all your treasure.\n"
str10 = "You got greedy and wanted more than you need and eventually lost it all.\n"
str11 = "You sprained your ankle during the walk. Sorry. Adventure over.\n"
str12 = "You ran into an inevitable roadblock. Sorry. Adventure over.\n"

item = [intro, start, resp, str1, str2, str3, str4, direction, resp3, transport, resp4,
        decisions, resp5, str5, resp6, str6, resp7, str7, str8, almost_there, resp8,
        str9, str10, str11, str12]

with open("mock.txt", "w") as f:
    f.write(item[0])
print(item[0])

file = open("mock.txt", "r")
s = file.read()
file.close()

ans = input(item[1])

with open("mock.txt", "a") as f:
    f.write(item[1])
if ans[0] == 'y':
    with open("mock.txt", "a") as f:
        f.write(item[2])
    print(item[3])
    with open("mock.txt", "a") as f:
        f.write(item[3])
elif ans == 'n':
    with open("mock.txt", "a") as f:
        f.write(item[4])
    print(item[4])
    with open("mock.txt", "a") as f:
        f.write(item[4])
else:
    print(item[5])
    with open("mock.txt", "a") as f:
        f.write(item[5])
    quit()
if ans == '[y]es' or ans == '[n]o':
    print(item[6])
    with open("mock.txt", "a") as f:
        f.write(item[6])
option1 = input(item[7]).lower()
with open("mock.txt", "a") as f:
    f.write(item[7])
if option1 == "l":

    with open("mock.txt", "a") as f:
        f.write(item[8])
    option2 = input(item[9]).lower()

    with open("mock.txt", "a") as f:
        f.write(item[9])

    if option2 == "p":
        with open("mock.txt", "a") as f:
            f.write(item[10])
        option3 = input(item[11]).lower()
        with open("mock.txt", "a") as f:
            f.write(item[11])
        if option3 == "b":
            with open("mock.txt", "a") as f:
                f.write(item[12])
            print(item[13])
            with open("mock.txt", "a") as f:
                f.write(item[13])
        elif option3 == "o":
            with open("mock.txt", "a") as f:
                f.write(item[14])
            print(item[15])
            with open("mock.txt", "a") as f:
                f.write(item[15])
        elif option3 == "p":
            with open("mock.txt", "a") as f:
                f.write(item[16])
            print(item[17])
            with open("mock.txt", "a") as f:
                f.write(item[17])
        else:
            print(item[18])
            with open("mock.txt", "a") as f:
                f.write(item[18])
        option4 = input(item[19]).lower()
        with open("mock.txt", "a") as f:
            f.write(item[19])
        if option4 == "h":
            with open("mock.txt", "a") as f:
                f.write(item[20])
            print(item[21])
            with open("mock.txt", "a") as f:
                f.write(item[21])
        else:
            print(item[22])
            with open("mock.txt", "a") as f:
                f.write(item[22])
    else:
        print(item[23])
        with open("mock.txt", "a") as f:
            f.write(item[23])
else:
    print(item[24])
    with open("mock.txt", "a") as f:
        f.write(item[24])



